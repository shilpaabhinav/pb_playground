﻿Feature: BookFreeAppraisal
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@mytag
Scenario: Book a free appraisal
	Given I navigate to Purplebricks site
	When I click on Book a free appraisal
	And I click on Enter address manually
	And I update all fields in Edit address page
	And click on Submit
	Then I verify the entered address
	When I click on Next pick a time for your appraisal button
	And I select date and time
	And I click on Final Step button 
	And I update all fields in Your details section
	And I click on Confirm appraisal
	Then I verify expert details and appointment details
