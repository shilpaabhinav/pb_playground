﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurpleBricksPlayGround.PageObjects
{
    class BookFreeValuationPage
    {

        private IWebDriver driver;

        public BookFreeValuationPage()
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//p[@class='address-text']")]
        IWebElement enterAddressManually = null;

        [FindsBy(How = How.Id, Using = "number")]
        IWebElement addressNumber = null;

        [FindsBy(How = How.Id, Using = "street")]
        IWebElement addresStreet = null;

        [FindsBy(How = How.Id, Using = "town")]
        IWebElement addressSuburb = null;

        [FindsBy(How = How.Id, Using = "country")]
        IWebElement addressState = null;

        [FindsBy(How = How.Id, Using = "postcode")]
        IWebElement addressPostcode = null;

        [FindsBy(How = How.CssSelector, Using = "")]
        IWebElement submitButton = null;


        /**
         * Step 1
         * */
        public void clickEnterAddresManually()
        {
            enterAddressManually.Click();
        }

        public void enterAddressFieldsManually(string numberValue, string streetValue, string suburbValue, string stateValue, string postcodeValue)
        {
            addressNumber.SendKeys(numberValue);
            addresStreet.SendKeys(streetValue);
            addressSuburb.SendKeys(suburbValue);
            addressState.SendKeys(stateValue);
            addressPostcode.SendKeys(postcodeValue);
        }

        public void submitAddress()
        {
            submitButton.Click();
        }

        /**
        * Step 2
        * */
        [FindsBy(How = How.XPath, Using = "//p[@class='address-text']")]
        IWebElement enteredAddress = null;

        [FindsBy(How = How.XPath, Using = "//button[@id = 'valuation-time']")]
        IWebElement pickAtimeButton = null;

        public void verificationOfAddress(string numberValue, string streetValue, string stateValue, string postcodeValue)
        {
            Assert.AreEqual(enteredAddress.Text, numberValue + ", " + streetValue + ", " + stateValue + ", " + postcodeValue);
        }

        public void verifyEnteredAddress()
        {
            Assert.AreEqual(enteredAddress, "37, Carpentaria Crescent, Point Cook, 3030");
            
        }

        public void clickPickATime()
        {
            this.pickAtimeButton.Click();
        }

        public void selectTimeAndDateButton()
        {

        }
    }
}
