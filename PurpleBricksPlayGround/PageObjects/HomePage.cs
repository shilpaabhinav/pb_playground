﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PurpleBricksPlayGround.PageObjects
{
    class HomePage
    {

        private IWebDriver driver = new ChromeDriver();
        public HomePage()
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = "div[data-reactid='18'] > a[href*='book-free-valuation']")]
        IWebElement bookFreeAppraisal = null;

        public void navigateToHomepage()
        {
            driver.Navigate().GoToUrl("https://pbuser:Lmeiub501!@qa-au.pbr.so/");
        }

        public void clickBookFreeAppraisalButton()
        {
            bookFreeAppraisal.Click();
            Thread.Sleep(3000);
        }
    }
}
