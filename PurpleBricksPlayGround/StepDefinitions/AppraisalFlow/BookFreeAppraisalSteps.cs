﻿using PurpleBricksPlayGround.PageObjects;
using System;
using TechTalk.SpecFlow;

namespace PurpleBricksPlayGround.StepDefinitions
{
    [Binding]
    public class BookFreeAppraisalSteps
    {
        private BookFreeValuationPage bookFreeValuationPage;
        private HomePage homePage;
        public BookFreeAppraisalSteps()
        {
            bookFreeValuationPage = new BookFreeValuationPage();
            homePage = new HomePage();
        }

        
        [When(@"I click on Enter address manually")]
        public void WhenIClickOnEnterAddressManually()
        {
            bookFreeValuationPage.clickEnterAddresManually();
        }
        
        [When(@"I update all fields in Edit address page")]
        public void WhenIUpdateAllFieldsInEditAddressPage()
        {
            bookFreeValuationPage.enterAddressFieldsManually("37", "Carpentaria Crescent" , "Point Cook", "Victoria", "3030");
        }
        
        [When(@"click on Submit")]
        public void WhenClickOnSubmit()
        {
            bookFreeValuationPage.submitAddress();
        }
        
        [When(@"I click on Next pick a time for your appraisal button")]
        public void WhenIClickOnNextPickATimeForYourAppraisalButton()
        {
            bookFreeValuationPage.selectTimeAndDateButton();
            bookFreeValuationPage.verifyEnteredAddress();
        }
        
        [When(@"I select date and time")]
        public void WhenISelectDateAndTime()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I click on Final Step button")]
        public void WhenIClickOnFinalStepButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I update all fields in Your details section")]
        public void WhenIUpdateAllFieldsInYourDetailsSection()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I click on Confirm appraisal")]
        public void WhenIClickOnConfirmAppraisal()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I verify the entered address")]
        public void ThenIVerifyTheEnteredAddress()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I verify expert details and appointment details")]
        public void ThenIVerifyExpertDetailsAndAppointmentDetails()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
