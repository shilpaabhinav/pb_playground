﻿using PurpleBricksPlayGround.PageObjects;
using System;
using TechTalk.SpecFlow;

namespace PurpleBricksPlayGround.StepDefinitions
{
    [Binding]
    public class SharedSteps
    {
        private HomePage homePage;
        public SharedSteps()
        {
            homePage = new HomePage();
        }

        [Given(@"I navigate to Purplebricks site")]
        public void GivenINavigateToPurplebricksSite()
        {
            homePage.navigateToHomepage();
        }
        [When(@"I click on Book a free appraisal")]
        public void WhenIClickOnBookAFreeAppraisal()
        {
            homePage.clickBookFreeAppraisalButton();
        }
    }
}
